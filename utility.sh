#!/bin/bash

get_file_env_var () {
   echo "$(grep -v '^#' .env | grep -e $1 | sed -e 's/.*=//')";
}

get_project_phid_arg () {
	curl -sk "$2phid.lookup" -d "api.token=$3" -d "names[0]=#$1" | \
    python3 -c "
import sys, json; 
result = json.load(sys.stdin)['result']; 
phid = None if not result else result['#$1']['phid']; 
print('' if not phid else f'--project {phid}');
"
}

get_response_code () {
   echo $(curl --write-out '%{http_code}' --silent --output /dev/null $1);
}

is_container_running () {
   is_running=$(docker inspect -f '{{.State.Running}}' $1 2>/dev/null);
   [ "$is_running" == "true" ] && echo "true" || echo "false";
}

open_url_when_available () {
   # TODO: find linux command which can accept name of browser optionally specified in $2 ( it's working for "open" on MacOS below )
   wait_until_url_available "$1";
   ( open ${2:+-a "$2"} "$1" || xdg-open "$1" || echo "Unable to automatically open '$1', try opening it in a browser" )
}

wait_until_url_available () {
   while ! [[ "$(get_response_code $1)" =~ ^(200|301)$ ]]; do sleep 1; done;
   sleep 0.5;
}

"$@"
Quickly spin up a local Dockerized DDD / Datasette instance, from scratch.

## Installation 

Ensure you have [Docker](https://www.docker.com/products/docker-desktop) installed.

Clone this repo:

    git clone https://gitlab.wikimedia.org/mhurd/ddd-docker.git

## Usage

Switch to the `ddd-docker` directory:

    cd ~/ddd-docker

Use `make` to spin up DDD / Datasette:
-   ```
    make conduittoken=#YOUR_CONDUIT_TOKEN_HERE# conduiturl=https://phabricator.wikimedia.org/api/ port=#YOUR_PORT_HERE# baseurl=#YOUR_BASE_URL_HERE#
     ```
    - Fetches the latest DDD into `~/ddd-docker/ddd/`
    - Spins up Docker containment to serve DDD / Datasette pages
    - Opens DDD / Datasette page
    - Provides VSCode [launch.json](https://gitlab.wikimedia.org/mhurd/ddd-docker/-/blob/main/.vscode/launch.json) debugging configuration

---

After running `make`, a call to `populate` is required to cache data from Phabricator.
-   ```
    make populate project=release-engineering-team
     ```
    - fetches transaction and project information into `~/ddd-docker/ddd/www/metrics.db` which allows DDD / Datasette to present charts and graphs. This command calls the `cachecolumns` and `map` commands seen below
    - `project` is optional

Use `cachecolumns` to fetch Phabricator column data:
-   ```
    make cachecolumns project=release-engineering-team
    ```
    - may be called periodically to keep cache in sync with Phabricator
    - `project` is optional

Use `map` to fetch Phabricator ticket data:
-   ```
    make map project=release-engineering-team
    ```
    - may be called periodically to keep cache in sync with Phabricator
    - `project` is optional

Use `dailyrepopulate` daily to keep DDD in sync with the latest from Phabricator.
-   ```
    make dailyrepopulate
     ```

---

See the [Makefile](https://gitlab.wikimedia.org/mhurd/ddd-docker/-/blob/main/Makefile) for other supported make commands.
FROM python:3.10.2-slim-buster

# Can increment this to cache-bust commands below if needed
ARG build=0

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE 1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED 1

# `DDD` uses CONDUIT_TOKEN and CONDUIT_URL (the Makefile now sets these in an .env file)
# ENV CONDUIT_TOKEN ""
# ENV CONDUIT_URL "http://docker-phabricator-wmf_phabricator_1:80/api/"
# ENV CONDUIT_URL "https://phabricator.wikimedia.org/api/"

COPY . /app

WORKDIR /app/ddd

SHELL ["/bin/bash", "-c"]

RUN pip3 install poetry && \
    poetry install && \
    poetry run pip3 install debugger debugpy
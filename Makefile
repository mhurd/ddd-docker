SHELL := /bin/bash

makefile_dir = $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
ddd_dir = "$(makefile_dir)/ddd"
www_dir = "$(ddd_dir)/www"

.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	make stop
	make remove
	make prepare
	make start

.PHONY: prepare
prepare:
	@mkdir $(ddd_dir); \
	cd $(ddd_dir); \
	git clone -b main https://gitlab.wikimedia.org/repos/releng/ddd.git . --depth=1;

.PHONY: stop
stop:
	-@cd $(makefile_dir); \
	docker-compose stop

.PHONY: remove
remove:
	-@if [ -d "$(ddd_dir)" ]; then \
		read -p "Are you sure you want to delete the ddd container and EVERYTHING in \"$(ddd_dir)\" (y/n)? " -n 1 -r; \
		echo ; \
		if [ "$$REPLY" = "y" ]; then \
			cd $(makefile_dir); \
			docker-compose down; \
			rm -rf $(ddd_dir); \
			rm $(makefile_dir)/runonce; \
			rm $(makefile_dir)/.env; \
		fi; \
	fi

.PHONY: start
start: runonce
	@cd $(makefile_dir); \
	if [ -f .env ]; then \
		docker-compose up -d; \
		make openmetricsdashboardpage; \
    elif [ "$(conduittoken)" ] && [ "$(conduiturl)" ]; then \
		echo "CONDUIT_TOKEN=$(conduittoken)" > .env; \
		echo "CONDUIT_URL=$(conduiturl)" >> .env; \
		echo "BASE_URL=$(baseurl)" >> .env; \
		echo "PORT=$(port)" >> .env; \
		docker-compose up -d; \
		make openmetricsdashboardpage; \
    else \
        echo "Required 'conduittoken' and/or 'conduiturl' parameters not supplied - see the README."; \
    fi

runonce:
	-@cd $(makefile_dir); \
	echo "One-time make commands may be placed here"; \
	touch runonce;

dashboard_port = $$(./utility.sh get_file_env_var 'PORT')
dashboard_url = "http://localhost:$(dashboard_port)"

.PHONY: openmetricsdashboardpage
openmetricsdashboardpage:
	-@$(makefile_dir)/utility.sh open_url_when_available $(dashboard_url);

.PHONY: restart
restart:
	make stop
	make start

# "make bash" for bash access to the ddd container.
.PHONY: bash
bash:
	docker-compose exec datasette-server bash

# Used to cause datasette to restart, such as when manually placing 'metrics.db' into ddd/www/
.PHONY: reloaddatasette
reloaddatasette:
	touch ./ddd/www/metadata.yaml

# cachecolumns only accepts project phid currently, not name, hence the call to get_project_phid_arg
cachecolumns:
	@conduittoken=$$(./utility.sh get_file_env_var 'CONDUIT_TOKEN'); \
	conduiturl=$$(./utility.sh get_file_env_var 'CONDUIT_URL'); \
	maybe_project=$$(./utility.sh get_project_phid_arg $$project $$conduiturl $$conduittoken); \
	docker-compose exec datasette-server /bin/bash -c "cd /app/ddd/www && poetry run dddcli metrics cache-columns $$maybe_project";

# map can accept project names or phids, but for consistency with cachecolumns get_project_phid_arg is used, so pass it project name not phid
map:
	@conduittoken=$$(./utility.sh get_file_env_var 'CONDUIT_TOKEN'); \
	conduiturl=$$(./utility.sh get_file_env_var 'CONDUIT_URL'); \
	maybe_project=$$(./utility.sh get_project_phid_arg $$project $$conduiturl $$conduittoken); \
	docker-compose exec datasette-server /bin/bash -c "cd /app/ddd/www && poetry run dddcli metrics map $$maybe_project";

populate:
	-@if [ -f "$(www_dir)/metrics.db" ]; then \
		read -p "Do you want to delete \"$(www_dir)/metrics.db\" (y/n)? " -n 1 -r; \
		echo ; \
		if [ "$$REPLY" = "y" ]; then \
			rm "$(www_dir)/metrics.db" || true; \
			make restart; \
		fi; \
	fi
	make cachecolumns
	make map
	make reloaddatasette
	make openmetricsdashboardpage

dailyrepopulate:
	make restart
	for i in {1..100}; do docker-compose exec datasette-server /bin/bash -c "cd /app/ddd/www && poetry run dddcli metrics map --linear --order=id --pages=5 --cursor-id all" && make reloaddatasette && sleep 15; done;
